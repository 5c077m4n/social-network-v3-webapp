import { Component, OnInit, Input } from '@angular/core';

import {LoginService} from '../../services/login.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
	@Input() private username: string;
	@Input() private password: string;
	constructor(private loginService: LoginService) {}
	ngOnInit() {}

	login() {
		if(!this.username || !this.password) return;
		else this.loginService.login(this.username, this.password)
			.subscribe(data => {
				if(!data) return;
				if(data.error) return console.error(data.error);
				if(data.token && data.expiration) {
					window.localStorage.setItem('user', JSON.stringify(data.user));
					window.localStorage.setItem('token', JSON.stringify(data.token));
					window.localStorage.setItem('expiration', JSON.stringify(data.expiration));
				}
			});
	}
}
