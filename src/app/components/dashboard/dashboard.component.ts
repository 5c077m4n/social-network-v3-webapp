import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import {UserService} from '../../services/user.service';
import {PostService} from '../../services/post.service';
import {LoginService} from '../../services/login.service';
import {RegisterService} from '../../services/register.service';
import {User} from '../../user';
import {Post} from '../../post';


@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
	private existingUser: Boolean = true;
	posts: Post[] = [];

	constructor(
		private userService: UserService,
		private postService: PostService
	) {}
	ngOnInit() {
		this.hasTokenExpired();
		if(this.isLoggedIn()) this.getUserPosts();
	}

	getUserPosts() {
		const username = JSON.parse(window.localStorage.getItem('user')).username;
		this.postService.getPosts(username)
			.subscribe(posts => this.posts);
	}

	logout(): void {
		localStorage.removeItem('user');
		localStorage.removeItem('token');
		localStorage.removeItem('expiration');
		console.log(`You've been logged out.`);
	}
	isLoggedIn(): Boolean {
		return (
			localStorage.getItem('user') &&
			localStorage.getItem('token') &&
			localStorage.getItem('expiration')
		)? true : false;
	}
	hasTokenExpired(): Boolean {
		if(new Date().getTime() > (+localStorage.getItem('expiration'))) {
			this.logout();
			return true;
		}
		else return false;
	}

	refresh(): void {
		window.location.reload();
	}
}
